#include "../include/screen.h"
#include "../include/types.h"
// GRAPHICS LIBRARY FOR SCRATCH OS


void writechar(char c, int txtcolor, int bgcolor,int x, int y)
{
	int color =  (bgcolor << 4) | txtcolor;;
	string vidmem = (string) 0xb8000;
	vidmem[(y * sw + x)*sd] = c;
	vidmem[(y * sw + x)*sd+1] = color;
}

void initcustom(char c, int tc, int bc, int startx, int starty)
{
	int px = startx;
	int py = starty;
	writechar(c, tc, bc, px, py);
}

void write_rect(int len, int wid, int startx, int starty, int color)
{
	int a;
	int b;
	for (a = 0; a != wid; a = a + 1)
	{
		for (b = 0; b != len; b = b + 1)
		{
			initcustom(0x23, color, color, b+startx, a+starty);
		}
	}
}

char get_char_g(int cx, int cy)
{
	char c;
	string vidmem = (string) 0xb8000;
	return vidmem[(cy * sw + cx)*sd];
}