#include "../include/kb.h"
#include "../include/isr.h"
#include "../include/idt.h"
#include "../include/util.h"
#include "../include/shell.h"
kmain()
{
	isr_install();
	clearScreen();
	set_screen_color(10, 0);
	print("Welcome to Scratch OS\n");
    launch_shell(0);    
}
